# ripv1

RIPv1 implementation in C, written as a coding challenge.

## Threads

The main program should start three threads:
  - One thread listens for incoming updates and processes them.  Should
    I create a new thread/process to process the update so that two
    updates arrive shortly after each other, the second does not get
    lost?
  - A second thread should keep track of sending out updates every
    thirty seconds (or just a function with an alarm()?).
  - A third thread should do the housekeeping: remove stale entries.