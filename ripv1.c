/* See also man routed */

#include <errno.h>
#include <inttypes.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>
#include "errexit.h"
#include "sockets.h"
#include "ripv1.h"

int sock;	/* Global variable containing the listening socket */

void *rip_listener(void *param) {
	char buf[MAXLENGTH];     /* Store the received data in this buffer */
	int flags = 0;           /* No flags needed for recvfrom()         */
	struct sockaddr from;    /* Source address and port                */
	/* 2020-03-30:
	 * NO IDEA WHAT THE BELOW LINE OF CODE IS SUPPOSED TO DO.
	 * I believe I fixed it by replacing "data" with "from". This makes
	 * more sense now.
	 */
	socklen_t fromlen = (socklen_t) sizeof(from);
                            /* Size of source address                 */
	int pl = 0;					 /* Packet length									 */
	int num_entries = 0;		 /* Number of updates (prefixes) received  */
	ripv1 *data;

	while (1) {
		/* Wait for an update to be received. */
		if ((pl = recvfrom(sock, buf, sizeof(buf), flags, &from, &fromlen)) < 0)
   		errexit("recvfrom(): %s\n", strerror(errno));

		/* Verify that the received update has a plausible size. */
		if ((pl - sizeof(ripv1_header)) % sizeof(ripv1_destination)) {
			fprintf(stderr, "Packet size incorrect: ignoring packet.\n");
			continue;
		}
		num_entries = (pl - sizeof(ripv1_header)) / sizeof(ripv1_destination);
		/* The received data stored in buf can contain up to 20 updates.
		 * So when processing the data, we cannot read past num_entries
		 * or we will be reading bogus memory.
		 */
		data = (ripv1 *) buf;

		/* Let's do some error checking on the header. */
		if ((data->rh.rh_version != 1) || (data->rh.rh_zero != 0)) {
			fprintf(stderr, "Header information incorrect: ignoring packet\n");
			continue;
		}
		if ((data->rh.rh_command != RIP_REQUEST)
				&& (data->rh.rh_command != RIP_RESPONSE)) {
			fprintf(stderr, "Unsupported command: ignoring packet\n");
   		continue;
		}

		/* Process each entry in the update. Perhaps we should do this in
		 * a new thread to keep this one lean and small?
		 */
		dump_packet(data);
			
	}
	return NULL;
}

void *rip_updater(void *param) {
	for (int i=0; i<5; i++) {
		puts("Inside rip_updater()");
		sleep(1);
	}
	return NULL;
}

void *rip_housekeeper(void *param) {
	for (int i=0; i<5; i++) {
		puts("Inside rip_housekeeper()");
		sleep(1);
	}
	return NULL;
}

void dump_packet(int size, ripv1 *data) {
	fprintf(stderr, "Packet size: %d\n", size);
	int num_entries = (size - sizeof(ripv1_header)) / sizeof(ripv1_destination);

	for (int i=0; i<num_entries; i++) {
		fprintf(stderr, "Prefix: %l", data->rd.rd_address);
	}
}


int main(int argv, char **argv) {

	int fd;
	int opt;

	while ((opt = getopt(argc, argv, "hd:")) != -1) {
		switch (opt) {
			case 'd':
				fd = fopen(optarg, "rb");
				if (fd < 0) {
					errexit("Could not open %s.\n", optarg);
				}
				fseek(fd, 0L, SEEK_END);
				int size = ftell(fd);
				rewind(fd);
				char buffer[MAXLENGTH];
				fread(buffer, sizeof(buffer), 1, fd);
				dump_packet(size, (ripv1) buffer);
				exit(0);
			case 'h':
			default:
				fprintf(stderr, "Usage: %s [-d file]\n", argv[0]);
				exit(1);
		}
	}

	/* Both the rip_listener() and the rip_updater() need access to the
	 * socket so we must create the socket before creating these
	 * threads. For this the variable 'sock' needs to be a global
	 * variable.
	 */
   sock = passive_socket(TRANSPORT, SERVICE);

	/* Create all threads. */
	pthread_t t_listen;
	if (pthread_create(&t_listen, NULL, rip_listener, NULL) == -1)
		errexit("Could not create listener thread.\n");
	pthread_t t_updater;
	if (pthread_create(&t_updater, NULL, rip_updater, NULL) == -1)
		errexit("Could not create updater thread.\n");
	pthread_t t_housekeep;
	if (pthread_create(&t_housekeep, NULL, rip_housekeeper, NULL) == -1)
		errexit("Could not create housekeeper thread.\n");

	/* Wait for all threads to finish before quiting. */
	void *result;
	if (pthread_join(t_listen, &result) == -1)
		errexit("Can't join listener thread");
	if (pthread_join(t_updater, &result) == -1)
		errexit("Can't join updater thread");
	if (pthread_join(t_housekeep, &result) == -1)
		errexit("Can't join housekeeper thread");
	
	/* Huray. Done. */
	exit(0);
}

