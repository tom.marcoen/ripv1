#define MAXLENGTH     512                       /* See RFC 1058, p.20 */
#define SERVICE 	    "router"
#define TRANSPORT     "udp"

#define RIP_REQUEST   0x01
#define RIP_RESPONSE  0x02

typedef struct {
	uint8_t rh_command;
	uint8_t rh_version;
	uint16_t rh_zero;
} ripv1_header;

typedef struct {
	uint16_t rd_family;
	uint16_t rd_zero1;
	uint32_t rd_address;
	uint64_t rd_zero2;
	uint32_t rd_metric;
} ripv1_destination;

typedef struct {
	ripv1_header rh;
	ripv1_destination rd[20];
} ripv1;

void *rip_listener(void *);
void *rip_updater(void *);
void *rip_housekeeping(void *);
void dump_packet(int size, ripv1 data);