#include <limits.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "errexit.h"

/* passive_socket()
 *
 * To accept connections, a socket is first created with socket(2), a
 * willingness to accept incoming connections and a queue limit for 
 * incoming connections are specified with listen(), and then the 
 * connections are accepted with accept(2). This function handles the 
 * first two items: socket() and listen().
 *
 * TODO: listen() is only needed for SOCK_STREAM and SOCK_SEQPACKET,
 * and has not yet been implemented.
 *
 * Arguments:
 * - char *transport - either "udp" or "tcp"
 * - char *service   - name of the protocol, e.g. "domain" for DNS
 *
 * Both arguments accept strings instead of numerical values to keep
 * the code readable: no weird numbers.
 * 2020-03-30: Well... and so you can do the lookup via /etc/services
 */
int passive_socket(const char *transport, const char *service)
{
	int sock;                /* file descriptor for the network socket */
   int type;                /* either SOCK_DGRAM or SOCK_STREAM       */
   struct protoent *ppe;    /* pointer to protocol entry              */
									 /*   see /usr/src/include/netdb.h         */
   struct servent *pse;     /* pointer to service entry               */
									 /*   see /usr/src/include/netdb.h         */
   struct sockaddr_in sin;  /* structure to hold the socket address   */
                            /*   see /usr/src/sys/netinet/in.h        */

   /* Initialize the socket address */
   memset(&sin, 0, sizeof(sin));
   sin.sin_family = AF_INET;
   sin.sin_addr.s_addr = INADDR_ANY; /* quad zero; weird extra struct */
                                     /*   for legacy reasons          */

	/* Validate the transport */
	if (strcmp(transport,"udp") == 0)
		type = SOCK_DGRAM;
	else if (strcmp(transport,"tcp") == 0)
		type = SOCK_STREAM;
	else
		errexit("No valid transport given.\n");
	if ((ppe = getprotobyname(transport)) == 0)
		errexit("Can't get the protocol number.\n");

	/* Validate the service */
	if ((pse = getservbyname(service, transport))) {
		/* Found the service */
		sin.sin_port = (in_port_t) pse->s_port;
	/* Check whether the string is just a number */
	} else if ((sin.sin_port = htons((uint16_t) strtol(service, NULL, 10))) == 0)
		errexit("No valid service given.\n");

	/* Create the socket and start listening for traffic */
	if ((sock = socket(PF_INET, type, ppe->p_proto)) < 0)
		errexit("Can't create socket.\n");

	return sock;
}

